const dataBaseHeroes = [
    {
        nick: 'Dendi',
        imgname: 'https://dota2.ru/img/esport/player/495.jpg?1470163995',
        team: 'B8',
        imgteam: 'https://dota2.ru/img/esport/team/5044.png?1599845414',
        country: 'Ukraina',
        money: '$ 381 872',
        games: '890',
        more: 'https://www.cybersport.ru/base/gamers/dendi/dota2',
        about: 'Даниил начал карьеру в 2007 году, в составе WG. В 2008-м представлял одну из сильнейших команд того времени - Kingsurf.int. С осени 2008 до конца 2010-го выступал за DTS. В декабре перешел в Natus Vincere и спустя несколько месяцев помог команде завоевать чемпионский титул The International 2011.',
    },
    {
        nick: 'ILTW',
        imgname: 'https://dota2.ru/img/esport/player/510.png?1618125624',
        team: 'Nigma',
        imgteam: 'https://dota2.ru/img/esport/team/4980.png?1597558959',
        country: 'russia',
        money: '$ 76 836',
        games: '1991',
        more: 'https://www.cybersport.ru/base/gamers/iltw7920/dota2',
        about: "Имя: Игорь ФилатовРодился: 29.10.1999 (21 год)Страна: Россия"
    },
    {
        nick: 'Solo',
        imgname: 'https://dota2.ru/img/esport/player/322.png?1566017859',
        team: 'NoTechis',
        imgteam: 'https://dota2.ru/img/esport/team/5472.png?1617701320',
        country: 'Russia',
        money: '$ 1 640 502',
        games: ' 529',
        more: 'https://www.cybersport.ru/base/gamers/solo/dota2'
    },
    {
        nick: 'Miracle-',
        imgname: 'https://dota2.ru/img/esport/player/535.png?1614074104',
        team: 'Nigma',
        imgteam: 'https://dota2.ru/img/esport/team/4980.png?1597558959',
        country: 'Irodaniya',
        money: '$4 788 991',
        games: ' 462',
        more: 'https://www.cybersport.ru/base/gamers/miracle-io/dota2'
    },
    {
        nick: 'gpk',
        imgname: 'https://dota2.ru/img/esport/player/2768.png?1612479845',
        team: 'VP',
        imgteam: 'https://dota2.ru/img/esport/team/72.png?1597437794',
        country: 'Russia',
        money: '$ 108 502',
        games: '1717',
        more: 'https://liquipedia.net/dota2/Gpk'
    },
    {
        nick: 'Topson',
        imgname: 'https://dota2.ru/img/esport/player/2176.png?1590652934',
        team: 'OG',
        imgteam: 'https://dota2.ru/img/esport/team/44.png?1597581461',
        country: 'Finland',
        money: '$ 5 513 607',
        games: '483',
        more: 'https://liquipedia.net/dota2/Topson'
    },
    {
        nick: 'Iceberg',
        imgname: 'https://dota2.ru/img/esport/player/244.png?1612496421',
        team: 'Na-Vi',
        imgteam: 'https://dota2.ru/img/esport/team/25.png?1597580927',
        country: 'Ukraine',
        money: '$ 205 277',
        games: '1019',
        more: 'https://www.cybersport.ru/base/gamers/iceberg4181/dota2'
    },
    {
        nick: 'Sumail',
        imgname: 'https://dota2.ru/img/esport/player/468.png?1590652902',
        team: 'OG',
        imgteam: 'https://dota2.ru/img/esport/team/44.png?1597581461',
        country: 'Pakistan',
        money: '$3 754 086',
        games: '1827',
        more: 'https://liquipedia.net/dota2/SumaiL'
    },
    {
        nick: 'Cooman',
        imgname: 'https://dota2.ru/img/esport/player/1403.png?1589711405',
        team: 'HellRss',
        imgteam: 'https://dota2.ru/img/esport/team/893.png?1596221012',
        country: 'Russia',
        money: '$78 682',
        games: '1453',
        more: 'https://www.cybersport.ru/base/gamers/cooman7929/dota2'
    },
    {
        nick: 'V-tune',
        imgname: 'https://dota2.ru/img/esport/player/2714.png?1612488762',
          team: 'Na-Vi',
        imgteam: 'https://dota2.ru/img/esport/team/25.png?1597580927',
        country: 'Ukraine',
        money: '$75 171',
        games: '1042',
        more: 'https://www.cybersport.ru/base/gamers/v-tune/dota2'
    },
]
const container = document.querySelector('.cont_cards');
const inputPlayers = document.querySelector('.inputPlayers');
const window_content = document.querySelector('.window_content');

window.addEventListener('load' , showCards(dataBaseHeroes));
function showCards(arr){
    const cards = arr.map(({about,nick,imgname,team,imgteam,country,money,games,more,}) => {
        return `
        <div class="GeneralBlockPlayers">
            <div class="playerBlock">
                <img src="${imgname}">
                <a href="${more}"><p>${nick}</p></a>
            </div>
            <div class="teamPlayers">
                <img src="${imgteam}">
                <p>${team}</p>
            </div>
            <ul>
                <li class="country"> ${country}</li>
            </ul>
            <ul>
                <li class="moneyPlayers"> ${money}</li>
            </ul>
            <ul>
                <li class="gamesPlayers"> ${games}</li>
            </ul>
            <ul>
            <li><button class="btnMore" type="button"data-toggle="modal" data-target="#staticBackdrop">Узнайте Больше</button></li>
            </ul>
        </div>
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">    
            <div class="modal-dialog">
                <div class="modal-content bg-dark" style="color: #fff;">
                    <div class="modal-header text-center">
                        <h5 class="modal-title" id="staticBackdropLabel">${nick}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                        <div class="modal-body">
                            ${about}
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        `

    }).join('')
    container.innerHTML  = cards;
}













inputPlayers.addEventListener('input' , e =>{
    const value = e.target.value.toUpperCase();
    const filtredArr = dataBaseHeroes.filter(({nick}) => nick.toUpperCase().includes(value));
    showCards(filtredArr);
});